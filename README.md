#Yew-Counter

A simple counter test [Yew](https://yew.rs/), a [WASM](https://webassembly.org/) framework built with [rust](https://www.rust-lang.org/).

To run the application you must have [trunk](https://trunkrs.dev/) installed, then run it:
```
trunk serve
```